#ifndef THIRDPARTY_TRAC_IK_INCLUDE_TRAC_IK_UTILS_H_
#define THIRDPARTY_TRAC_IK_INCLUDE_TRAC_IK_UTILS_H_

// standard includes
#include <stdlib.h>

#include <ostream>
#include <string>
#include <vector>

#include <kdl/chain.hpp>
#include <kdl/jntarray.hpp>
#include <urdf/model.h>

namespace rtr {

namespace TRAC_IK {

// TO-DO (RTR): Migrate this to ROS 2
/*bool LoadModelOverride(const ros::NodeHandle& nh, const std::string& robot_description,
                       urdf::Model& model);*/

bool InitKDLChain(const urdf::Model& model, const std::string& base_name,
                  const std::string& tip_name, KDL::Chain& chain,
                  std::vector<std::string>& link_names, std::vector<std::string>& joint_names,
                  KDL::JntArray& joint_min, KDL::JntArray& joint_max);

}  // namespace TRAC_IK

}  // namespace rtr

namespace KDL {

std::ostream& operator<<(std::ostream& o, const JntArray& q);
std::ostream& operator<<(std::ostream& o, const Vector& v);
std::ostream& operator<<(std::ostream& o, const Rotation& R);
std::ostream& operator<<(std::ostream& o, const Frame& f);

}  // namespace KDL

#endif
